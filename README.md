# mytrivy

## Ejecución
```
$ docker run --rm -it registry.gitlab.com/sro6474241/challenge1/mytrivy:1.0.0 trivy image -s HIGH,CRITICAL registry.gitlab.com/sro6474241/challenge1/myowncloud:1.0.0
```
## Notas
- Similar uso que: https://hub.docker.com/r/aquasec/trivy
- Documentación: https://aquasecurity.github.io/trivy/v0.46/docs/
FROM ubuntu:23.10 AS bin
RUN apt-get update -y && \
apt-get install --no-install-recommends -y \
    wget=1.21.3-1ubuntu1 \
    ca-certificates=20230311ubuntu1 && \
wget --progress=dot:giga \
    https://github.com/aquasecurity/trivy/releases/download/v0.46.0/trivy_0.46.0_Linux-64bit.tar.gz && \
tar -xzf ./trivy_0.46.0_Linux-64bit.tar.gz

FROM alpine:3.18.4
COPY --from=bin trivy /bin/
COPY --from=bin contrib /contrib
CMD ["trivy"]
